package de.datev.wowlist;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.UUID;

@Entity
@Getter
public class Subtask {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column
    private String description;

    @ManyToOne
    private Todo todo;

    public Subtask(String description, Todo todo) {
        this.description = description;
        this.todo = todo;
    }

    Subtask(){}
}
