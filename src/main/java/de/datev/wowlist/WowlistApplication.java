package de.datev.wowlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class WowlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(WowlistApplication.class, args);
	}

}
