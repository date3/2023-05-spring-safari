package de.datev.wowlist;

import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TodoService {

    TodoRepository todoRepository;

    TodoService(TodoRepository todoRepository){
        this.todoRepository = todoRepository;
    }

    public Todo createTodo(Todo todo) {
        Todo newTodo = new Todo(todo.getDueDate(), todo.getDescription());
        return todoRepository.save(newTodo);
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public void deleteAllTodos() {
        todoRepository.deleteAll();
    }

    public Todo getTodoById(UUID id) {
        Optional<Todo> foundTodo = todoRepository.findById(id);
        return foundTodo.orElse(null);
    }

    public void changeTodo(UUID id, String description, Instant dueDate) {
        Todo todoById = getTodoById(id);
        todoById.markAsDone(description, dueDate);

        todoRepository.save(todoById);
    }

    public List<Todo> findByDescription(String description) {
        return todoRepository.findByDescriptionContains(description);
    }

    public Todo createSubtaskForTodo(UUID todoId, String description) {
        Optional<Todo> foundTodo = todoRepository.findById(todoId);
        if(foundTodo.isPresent()){
            Todo todo = foundTodo.get();
            todo.addSubtaskWithDescription(description);

            return todoRepository.save(todo);
        }
        return null;
    }
}
