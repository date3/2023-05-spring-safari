package de.datev.wowlist;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class TodoHealthIndicator implements HealthIndicator {

    private TodoRepository todoRepository;

    public TodoHealthIndicator(TodoRepository todoRepository){
        this.todoRepository = todoRepository;
    }

    @Override
    public Health health() {
        long todoCount = todoRepository.count();
        if (todoCount == 0) {
            return Health.down().build();
        }
        if (todoCount > 10) {
            return Health.outOfService().build();
        }

        return Health.up().build();
    }
}
