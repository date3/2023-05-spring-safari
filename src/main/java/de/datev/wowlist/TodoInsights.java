package de.datev.wowlist;

import lombok.Data;

import java.time.Instant;

@Data
public class TodoInsights {

    long count;
    Instant lastInsert;
}
