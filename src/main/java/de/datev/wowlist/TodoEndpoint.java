package de.datev.wowlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.web.annotation.WebEndpoint;
import org.springframework.stereotype.Component;

@Component
@WebEndpoint(id="todos")
public class TodoEndpoint {

    @Autowired
    TodoRepository todoRepository;

    @ReadOperation
    public TodoInsights getTodoInfo(){
        TodoInsights insights = new TodoInsights();
        insights.setCount(todoRepository.count());
        insights.setLastInsert(todoRepository.findFirstByOrderByCreatedAtDesc().getCreatedAt());
        return insights;
    }
}
