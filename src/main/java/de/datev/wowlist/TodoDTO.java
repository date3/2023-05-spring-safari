package de.datev.wowlist;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
public class TodoDTO {

    private UUID id;
    private Instant dueDate;
    private String description;

    private boolean done;

    public TodoDTO(UUID id, String description, Instant dueDate) {
        this.id = id;
        this.description = description;
        this.dueDate = dueDate;
    }
}
