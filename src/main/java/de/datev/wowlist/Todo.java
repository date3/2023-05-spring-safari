package de.datev.wowlist;

import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Getter
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Future
    @Temporal(TemporalType.TIMESTAMP)
    private Instant dueDate;

    @Size(max = 500)
    @Column(length = 1000)
    private String description;

    @Column
    private boolean done;

    @CreationTimestamp
    private Instant createdAt;

    @UpdateTimestamp
    private Instant updatedAt;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    private List<Subtask> subtask = new ArrayList<>();

    public Todo(Instant now, String description) {
        this.dueDate = now;
        this.description = description;
        this.done = false;
    }

    public Todo(String description) {
        this.dueDate = Instant.now();
        this.description = description;
        this.done = false;
    }

    Todo(){}

    public void markAsDone(String description, Instant dueDate) {
        this.description = description;
        this.dueDate = dueDate;
        this.done = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addSubtaskWithDescription(String description) {
        Subtask newSubtask = new Subtask(description, this);
        subtask.add(newSubtask);
    }
}
