package de.datev.wowlist;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfiguration {


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity security) throws Exception {
        return security
                .headers().frameOptions().disable().and().csrf().disable()
                .authorizeHttpRequests().requestMatchers(new AntPathRequestMatcher("/todos/**")).permitAll()
                .and()
                .authorizeHttpRequests().requestMatchers(new AntPathRequestMatcher("/h2-console/**")).permitAll()
                .and()
                .formLogin()
                .and()
                .build();
    }

    @Bean
    public UserDetailsManager users(){
        User.UserBuilder userBuilder = User.withDefaultPasswordEncoder();
        UserDetails user = userBuilder.username("donald").password("duck").roles("USER").build();

        return new InMemoryUserDetailsManager(user);
    }
}
