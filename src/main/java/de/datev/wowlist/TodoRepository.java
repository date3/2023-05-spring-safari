package de.datev.wowlist;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface TodoRepository extends JpaRepository<Todo, UUID> {

    //@Query("from Todo t where t.description = ?1")
    List<Todo> findByDescriptionContains(String description);

    List<Todo> findByDueDateBefore(Instant now);

    Todo findFirstByOrderByCreatedAtDesc();
}
