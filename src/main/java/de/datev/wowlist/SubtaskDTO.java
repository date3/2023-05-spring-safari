package de.datev.wowlist;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubtaskDTO {

    @NotEmpty
    private String description;
}
