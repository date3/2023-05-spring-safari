package de.datev.wowlist;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService){
        this.todoService = todoService;
    }

    @GetMapping("/todos")
    public List<TodoDTO> getAllTodos() {
        return todoService.getAllTodos()
                .stream()
                .map(TodoController::mapToDTO).toList();
    }

    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDTO createTodo(@RequestBody TodoRequestDto todo) {
        Todo mappedTodo = new Todo(todo.getDueDate(), todo.getDescription());

        Todo mappedDto = todoService.createTodo(mappedTodo);
        return mapToDTO(mappedDto);
    }

    @PutMapping("/todos/{id}")
    public TodoDTO changeTodo(@PathVariable UUID id, @RequestBody TodoRequestDto request){
        todoService.changeTodo(id, request.getDescription(), request.getDueDate());

        Todo todoById = todoService.getTodoById(id);

        return mapToDTO(todoById);
    }

    @GetMapping("/todos/{id}")
    public ResponseEntity<TodoDTO> getSingleTodo(@PathVariable UUID id) {
        Todo foundTodo = todoService.getTodoById(id);

        if(foundTodo == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(mapToDTO(foundTodo));
    }

    @GetMapping("/todos/query")
    public List<Todo> findByDescription(@RequestParam String description){
        return todoService.findByDescription(description);
    }

    @PostMapping("/todos/{id}/subtask")
    public TodoDTO createSubtask(@PathVariable UUID id, @Valid @RequestBody SubtaskDTO subtaskDTO){
        Todo changedTodo = todoService.createSubtaskForTodo(id, subtaskDTO.getDescription());

        return mapToDTO(changedTodo);
    }

    private static TodoDTO mapToDTO(Todo todo1) {
        return new TodoDTO(todo1.getId(), todo1.getDescription(), todo1.getDueDate());
    }
}
