package de.datev.wowlist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class TodoRequestDto {
    private String description;
    private Instant dueDate;

}
