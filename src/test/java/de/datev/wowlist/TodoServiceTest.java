package de.datev.wowlist;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


public class TodoServiceTest {

    private TodoService todoService;

    private TodoRepository todoRepository;

    @BeforeEach
    public void createTodoService(){
        todoRepository = Mockito.mock(TodoRepository.class);
        todoService = new TodoService(todoRepository);
    }

    @Test
    public void createTodo_todoIsCreated(){
        when(todoRepository.save(any())).thenReturn(new Todo("Tee kochen"));

        Instant now = Instant.now();
        Todo todo = new Todo(now, "Tee kochen");
        Todo createdTodo = todoService.createTodo(todo);

        assertEquals(createdTodo.getDescription(), "Tee kochen");
    }

    @Test
    public void getAllTodos_noTodos_emptyList(){
        var todos = todoService.getAllTodos();
        assertTrue(todos.isEmpty());
    }

    @Test
    public void getAllTodos_oneExistingTodo_returnsOneTodo(){
        when(todoRepository.findAll()).thenReturn(List.of(new Todo("Tee kochen")));

        Todo todo = new Todo(Instant.now(), "Tee kochen");

        todoService.createTodo(todo);
        var todos = todoService.getAllTodos();
        assertEquals(1, todos.size());
    }

    @Test
    public void deleteAllTodos_withOneTodo_allTodosDeleted(){
        Todo todo = new Todo(Instant.now(), "Tee kochen");

        todoService.createTodo(todo);
        todoService.deleteAllTodos();

        assertEquals(0, todoService.getAllTodos().size());
    }
}
