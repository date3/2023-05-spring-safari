package de.datev.wowlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Instant;
import java.util.List;

@DataJpaTest
public class TodoRepositoryTest {

    @Autowired
    TodoRepository todoRepository;

    @Test
    public void findByDueDate_beforeGivenDueDate(){
        Instant now = Instant.now();
        List<Todo> dueTodos = todoRepository.findByDueDateBefore(now);

        Assertions.assertTrue(dueTodos.isEmpty());
    }

}
