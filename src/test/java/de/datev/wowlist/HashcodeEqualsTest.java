package de.datev.wowlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;

public class HashcodeEqualsTest {

    @Test
    public void koan() {
        String name1 = new String("c");
        String name2 = new String("c");

        Assertions.assertTrue(name1.equals(name2));
    }

    /**
     * This tests demonstrates why it is important to have a proper hashcode/equals implementation on
     * all entities.
     */
    @Test
    public void regression() {
        Instant now = Instant.now();
        String description = "test";

        ArrayList<Todo> db = new ArrayList<>();

        Todo newTodo = new Todo(now, description);
        // todo is saved in db
        db.add(newTodo);

        // time goes by... and a new object with the same properties is created
        Todo todoToDelete = new Todo(now, description);

        // are we able to delete the todo?
        Assertions.assertTrue(db.remove(todoToDelete));
        Assertions.assertTrue(db.isEmpty());
    }

}
