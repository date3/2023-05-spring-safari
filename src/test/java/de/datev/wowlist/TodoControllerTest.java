package de.datev.wowlist;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    MockMvc mockMvc;

    TodoService todoService;
    private final ObjectMapper objectMapper;

    @Autowired
    public TodoControllerTest(MockMvc mockMvc, TodoService todoService, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.todoService = todoService;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    public void clearDatabase() {
        todoService.deleteAllTodos();
    }

    @Test
    public void getAllTodos_noExistingTodos_emptyListIsReturned() throws Exception {
        mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));
    }

    @Test
    public void createTodo_todoCreated_oneTodoReturned() throws Exception {
        mockMvc.perform(post("/todos")
                        .content(todoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", Matchers.equalTo("Tee kochen")));

        mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)));
    }

    @Test
    public void createTodo_invalidWithDescriptionLongerThan500_returns500() throws Exception {
        mockMvc.perform(post("/todos").content(invalidTodoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private String invalidTodoAsString() {
        return """
                {
                    "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea",
                    "dueDate": "2023-05-25T08:17:47.720Z"
                }
                """;
    }

    @Test
    public void getSingleTodo_validTodo_todoIsReturned() throws Exception {
        String contentAsString = mockMvc.perform(post("/todos")
                        .content(todoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        TodoDTO todoDTO = objectMapper.readValue(contentAsString, TodoDTO.class);

        mockMvc.perform(get("/todos/" + todoDTO.getId()))
                .andExpect(status().isOk());
    }

    @Test
    public void getSingleTodo_todoNotFound_returns404() throws Exception {
        mockMvc.perform(post("/todos")
                        .content(todoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(get("/todos/e58ed763-928c-4155-bee9-fdbabadc15f3"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void changeTodo_existingTodo_todohasChanged() throws Exception {
        String contentAsString = mockMvc.perform(post("/todos")
                        .content(todoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        TodoDTO todoDTO = objectMapper.readValue(contentAsString, TodoDTO.class);

        mockMvc.perform(put("/todos/" + todoDTO.getId())
                        .content(changedTodo())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(get("/todos/" + todoDTO.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("Bier brauen"));
    }

    @Test
    public void findTodoByDescription_todoExists_todoIsReturned() throws Exception {
        mockMvc.perform(post("/todos")
                .content(todoAsString())
                .contentType(MediaType.APPLICATION_JSON));

        mockMvc.perform(get("/todos/query").param("description", "Tee kochen"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value("Tee kochen"));
    }

    @Test
    public void createSubtask_existingTodo_subtaskCreated() throws Exception {
        String contentAsString = mockMvc.perform(post("/todos")
                        .content(todoAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();

        TodoDTO todoDTO = objectMapper.readValue(contentAsString, TodoDTO.class);

        mockMvc.perform(post("/todos/{id}/subtask", todoDTO.getId())
                        .content(subtaskAsString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String subtaskAsString() {
        return """
                {
                    "description": "Wasser aufkochen"
                }        
                """;
    }


    private String todoAsString() {
        return """
                {
                    "description": "Tee kochen",
                    "dueDate": "2023-05-25T08:17:47.720Z"
                }
                """;
    }

    private String changedTodo() {
        return """
                {
                    "description": "Bier brauen",
                    "done": true,
                    "dueDate": "2023-05-25T08:17:47.720Z"
                }
                """;
    }

}
